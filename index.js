const http = require('http');

class Server {
  #hostname;
  #port;
  #eventEmitter;
  
  constructor(config) {
    this.#hostname = config.hostname;
    this.#port = config.port;
    this.#eventEmitter = config.eventEmitter;    
  }

  start() {
    http.createServer((req, res) => {
        this.#eventEmitter.emit('newReq', req, res);
      })
      .listen(this.#port, this.#hostname, ()=> {
        console.log(`Server is running at ${this.#hostname}:${this.#port}`);
      });
  }
}

module.exports = Server;